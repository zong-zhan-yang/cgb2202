package cn.tedu.boot31.mapper;

import cn.tedu.boot31.entity.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductMapper {
//    insert into product values(null,‘手机’,500,100);
    @Insert("insert into product values(null,#{title},#{price},#{saleCount})")
    void insert(Product product);

    @Select("select * from product")
    @Result(column = "sale_count",property = "saleCount")
    List<Product> select();

    @Delete("delete from product where id=#{id}")
    void deleteById(int id);

    @Update("update product set title=#{title},price=#{price}" +
            ",sale_count=#{saleCount} where id=#{id}")
    void update(Product product);

}
