package cn.tedu.boot31.controller;

import cn.tedu.boot31.entity.Product;
import cn.tedu.boot31.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//此注解相当于在每一个方法上面添加ResponseBody注解
@RestController
public class ProductController {
    @Autowired(required = false)
    ProductMapper mapper;

    @RequestMapping("/insert")
    public String insert(Product product){
        System.out.println("product = " + product);
        mapper.insert(product);
        return "添加完成!<a href='/'>返回首页</a>";
    }
    @RequestMapping("/select")
    public String select(){
        List<Product> list = mapper.select();
        String html = "<table border=1>";
        html+="<caption>商品列表</caption>";
        html+="<tr><th>id</th><th>标题</th><th>价格</th><th>销量</th><th>操作</th></tr>";
        //把商品数据装进表格标签里面
        for (Product p: list) {
            html+="<tr>";
            html+="<td>"+p.getId()+"</td>";
            html+="<td>"+p.getTitle()+"</td>";
            html+="<td>"+p.getPrice()+"</td>";
            html+="<td>"+p.getSaleCount()+"</td>";
            html+="<td><a href='/delete?id="+p.getId()+"'>删除</a></td>";
            html+="</tr>";
        }
        html+="</table>";
        return html;
    }
    @RequestMapping("/delete")
    public String delete(int id){
        System.out.println("id = " + id);
        mapper.deleteById(id);
        return "删除完成!<a href='/select'>返回列表页面</a>";
    }

    @RequestMapping("/update")
    public String update(Product product){
        System.out.println("product = " + product);
        mapper.update(product);
        return "修改完成!<a href='/select'>返回列表页面</a>";
    }
}
