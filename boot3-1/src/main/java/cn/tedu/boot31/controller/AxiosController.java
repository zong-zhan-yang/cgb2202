package cn.tedu.boot31.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.PrintWriter;

@RestController
public class AxiosController {


    @RequestMapping("/hello1Axios")
    public String hello1(String info){
        return "请求成功! info="+info;
    }

    //@RequestBody注解作用, 当客户端发出post请求并且提交的是自定义对象时 服务器端
    //接收参数必须使用此注解 否则得不到传递过来的参数.
    @RequestMapping("/hello2Axios")
    public String hello2(@RequestBody String info){
        return "请求成功! info="+info;
    }

}







