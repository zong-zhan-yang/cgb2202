package cn.tedu.boot21.controller;

import cn.tedu.boot21.entity.Emp;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Controller
public class HelloController {
//    @RequestMapping("/hello")
//    public void hello(HttpServletResponse response) throws IOException {
//        response.setContentType("text/html;charset=utf-8");
//        PrintWriter pw = response.getWriter();
//        pw.print("测试成功!");
//        pw.close();
//    }
    @RequestMapping("/hello")
    @ResponseBody //SpringMVC框架提供的注解,作用:可以通过返回值的方式给客户端响应数据
    public String hello(){
        return "测试成功!222";
    }
    @RequestMapping("/param1")
    @ResponseBody
    public String param1(HttpServletRequest request){
        String info = request.getParameter("info");
        return "接收到了参数:"+info;
    }
    @RequestMapping("/param2")
    @ResponseBody
    public String param2(String name,int age){
        return "接收到参数:"+name+"年龄:"+age;
    }

    @RequestMapping("/param3")
    @ResponseBody
    public String param3(Emp emp){
        return emp.toString();
    }

}
