package cn.tedu.boot321;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot321Application {

    public static void main(String[] args) {
        SpringApplication.run(Boot321Application.class, args);
    }

}
