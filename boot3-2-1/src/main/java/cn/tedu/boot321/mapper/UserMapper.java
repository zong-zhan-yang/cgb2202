package cn.tedu.boot321.mapper;

import cn.tedu.boot321.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper {
    //如果返回值为一个对象而非List集合 要求查询回来的结果必须是0条或1条
    //如果出现了大于1条的情况会报错
    @Select("select * from user where username=#{username}")
    User selectByUsername(String username);

    @Insert("insert into user values(null,#{username},#{password},#{nick})")
    void insert(User user);

}
