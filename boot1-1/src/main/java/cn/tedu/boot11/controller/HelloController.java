package cn.tedu.boot11.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Controller
public class HelloController {
    //http://localhost:8080/hello
    @RequestMapping("/hello")
    public void hello(HttpServletResponse response) throws IOException {
        //设置响应类型  告诉客户端服务器响应的是什么类型的内容 和字符集
        response.setContentType("text/html;charset=utf-8");
        //得到输出对象    异常抛出
        PrintWriter pw = response.getWriter();
        //输出数据
        pw.print("服务器接收到了请求!<h1>测试成功</h1>");
        //关闭资源
        pw.close();
    }
}
