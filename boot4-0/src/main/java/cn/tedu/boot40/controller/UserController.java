package cn.tedu.boot40.controller;

import cn.tedu.boot40.entity.User;
import cn.tedu.boot40.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired(required = false)
    UserMapper mapper;

    @RequestMapping("/reg")
    public int reg(@RequestBody User user){
        System.out.println("user = " + user);
        //通过输入的用户名查询用户信息
        User u = mapper.selectByUsername(user.getUsername());
        if (u!=null){
            return 2;//代表用户名已存在
        }
        //把输入的用户信息保存
        mapper.insert(user);
        return 1;//代表注册成功
    }

    @RequestMapping("/login")
    public int login(@RequestBody User user){
        //通过输入的用户名查询出数据库里面对应的数据
        User u = mapper.selectByUsername(user.getUsername());
        if (u!=null){
            //拿用户输入的密码和数据库中正确的密码作比较
            if (user.getPassword().equals(u.getPassword())){
                return 1;//登录成功
            }
            return 3;// 密码错误
        }
        return 2;//用户名不存在
    }
}
