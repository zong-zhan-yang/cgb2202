package cn.tedu.boot23.mapper;

import cn.tedu.boot23.entity.Hero;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface HeroMapper {
    @Insert("insert into hero values(null,#{name},#{money})")
    void insert(Hero hero);

    @Delete("delete from hero where name=#{name}")
    void delete(String name);

    @Update("update hero set name=#{name},money=#{money} where id=#{id}")
    void update(Hero hero);

    @Select("select * from hero")
    List<Hero> select();
}
