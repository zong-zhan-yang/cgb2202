package cn.tedu.boot23.controller;

import cn.tedu.boot23.entity.Hero;
import cn.tedu.boot23.mapper.HeroMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
//此注解相当于在每一个方法上面都添加了一个@ResponseBody
@RestController
public class HeroController {
    @Autowired(required = false)
    HeroMapper mapper;

    @RequestMapping("/add")
    public String add(Hero hero){
        mapper.insert(hero);
        return "添加完成!<a href='/'>返回首页</a>";
    }

    @RequestMapping("/delete")
    public String delete(String name){
        mapper.delete(name);
        return "删除完成!<a href='/'>返回首页</a>";
    }
    @RequestMapping("/update")
    public String update(Hero hero){
        mapper.update(hero);
        return "修改完成!<a href='/'>返回首页</a>";
    }

    @RequestMapping("/select")
    public String select(){
        List<Hero> list = mapper.select();
        return list.toString();
    }

}
