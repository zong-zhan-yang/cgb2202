package cn.tedu.boot51.mapper;

import cn.tedu.boot51.entity.Weibo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WeiboMapper {
    @Insert("insert into weibo values" +
            "(null,#{content},#{url},#{nick},#{created},#{userId})")
    void insert(Weibo weibo);
}
