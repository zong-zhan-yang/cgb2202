package cn.tedu.boot51.controller;

import cn.tedu.boot51.entity.User;
import cn.tedu.boot51.entity.Weibo;
import cn.tedu.boot51.mapper.WeiboMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Date;

@RestController
public class WeiboController {
    @Autowired(required = false)
    WeiboMapper mapper;
    @RequestMapping("/insert")
    public int insert(@RequestBody Weibo weibo, HttpSession session){
        //得到当前登录的用户对象
        User u = (User) session.getAttribute("user");
        if (u==null){ return 2;//代表未登录
         }
        //new Date()得到当前的系统时间
        weibo.setCreated(new Date());
        //把当前登录的用户信息 添加到weibo对象中
        weibo.setUserId(u.getId());
        weibo.setNick(u.getNick());
        System.out.println("weibo = " + weibo);
        mapper.insert(weibo);
        return 1;//代表发布微博成功!
    }
}
