package cn.tedu;

import com.alibaba.druid.support.spring.stat.annotation.Stat;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Demo09 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入用户名");
        String username = sc.nextLine();
        System.out.println("请输入密码");
        String password = sc.nextLine();
        System.out.println("请输入昵称");
        String nick = sc.nextLine();
        //把得到的3个信息保存到user表   参考Demo06
        try (Connection conn = DBUtils.getConn()){
            Statement s = conn.createStatement();
            s.executeUpdate("insert into user values(null,'"
                    +username+"','"+password+"','"+nick+"')");
            System.out.println("执行完成!");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
