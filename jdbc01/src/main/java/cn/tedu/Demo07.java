package cn.tedu;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Demo07 {
    public static void main(String[] args) {
        //把英雄表的所有信息查询出来并在控制台输出
        try (Connection conn = DBUtils.getConn()){
            Statement s = conn.createStatement();
            //执行查询SQL
            ResultSet rs = s.executeQuery("select * from hero");
            while(rs.next()){
                int id = rs.getInt(1);
                String name = rs.getString(2);
                int money = rs.getInt(3);
                System.out.println(id+":"+name+":"+money);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}
