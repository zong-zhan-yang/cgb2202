package cn.tedu;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Demo06 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("请输入英雄名");
        String name = sc.nextLine();
        System.out.println("请输入价格");
        int money = sc.nextInt();
        //获取数据库连接
        try (Connection conn = DBUtils.getConn()){
            Statement s = conn.createStatement();
            s.executeUpdate("insert into hero values(null,'"+name+"',"+money+")");
            System.out.println("执行完成!");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
