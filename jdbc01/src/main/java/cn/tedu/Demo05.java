package cn.tedu;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Demo05 {
    //添加main 方法 方法中创建一个hero表 有id,name,money价格 id自增
    //需要用上DBUtils工具类
    public static void main(String[] args) {
        try (Connection conn = DBUtils.getConn()){
            Statement s = conn.createStatement();
            s.execute("create table hero" +
                    "(id int primary key auto_increment,name varchar(50),money int)");
            System.out.println("执行完成!");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


}
