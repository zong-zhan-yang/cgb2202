package cn.tedu;

import java.sql.*;

public class Demo03 {
    public static void main(String[] args) throws SQLException {
        Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/empdb?characterEncoding=utf8&serverTimezone=Asia/Shanghai&useSSL=false",
                "root","root");
        Statement s = conn.createStatement();
        //执行插入数据SQL
//        s.executeUpdate("insert into emp(name) values('tom')");
        //执行修改的SQL
//        s.executeUpdate("update emp set name='jerry' where name='tom'");
        //执行删除的SQL
//        s.executeUpdate("delete from emp where name='jerry'");
        //执行查询的SQL
         ResultSet rs = s.executeQuery("select name,sal from emp");
         //遍历结果集中的数据
        while(rs.next()){
            //获取游标指向的这条数据的某个字段的值,通过字段名获取
//            String name = rs.getString("name");
//            double sal = rs.getDouble("sal");
            //通过查询回来数据的位置获取数据
            String name = rs.getString(1);
            double sal = rs.getDouble(2);
            System.out.println(name+":"+sal);
        }
        System.out.println("执行完成!");
        conn.close();
    }
}
