package cn.tedu.boot41.controller;

import cn.tedu.boot41.entity.Product;
import cn.tedu.boot41.mapper.ProductMapper;
import jdk.nashorn.internal.runtime.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductController {
    @Autowired(required = false)
    ProductMapper mapper;
    @RequestMapping("/insert")
    public void insert(@RequestBody Product product){
        System.out.println("product = " + product);
        mapper.insert(product);
    }

    @RequestMapping("/select")
    public List<Product> select(){
        // SpringMVC框架当发现返回值类型为集合或自定义的对象类型时,
        //会将集合或对象转成JSON格式的字符串,然后再将JSON格式字符串转成二进制数据进行网络传输
        //[{"id":1,"title":"阿迪袜子","price":10.0,"saleCount":1000},{"id":3,"title":"裤子","price":50.0,"saleCount":400},{"id":4,"title":"袜子","price":5.0,"saleCount":100}]
        List<Product> list = mapper.select();

        return list;
    }
    @RequestMapping("/delete")
    public void delete(int id){
        System.out.println("id = " + id);
        mapper.deleteById(id);
    }

    @RequestMapping("/selectById")
    public Product selectById(int id){
        System.out.println("id = " + id);
        //当SpringMVC框架发现返回的是一个自定义对象会自动转成JSON字符串再转成二进制进行网络传输
        return mapper.selectById(id);
    }

    @RequestMapping("/update")
    public void update(@RequestBody Product product){
        mapper.update(product);
    }



}
