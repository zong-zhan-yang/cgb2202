package cn.tedu.boot22.mapper;

import cn.tedu.boot22.entity.Hero;
import org.apache.ibatis.annotations.*;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

@Mapper
public interface HeroMapper {
    //#{xxx}会从下面方法的参数列表中找到同名的变量, 如果找不到同名变量则进入到对象里面查找
    //同名的get方法   Mybatis框架会根据此方法声明生成具体的实现类实现此方法,方法内部就是jdbc代码
    @Insert("insert into hero values(null,#{name},#{money})")
    void insert(Hero hero);

    @Delete("delete from hero where name=#{name}")
    void deleteByName(String name);

    @Update("update hero set name=#{name},money=#{money} where id=#{id}")
    void update(Hero hero);

    @Select("select * from hero")
    List<Hero> select();
}
