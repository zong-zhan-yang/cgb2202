package cn.tedu.boot22.controller;

import cn.tedu.boot22.entity.Hero;
import cn.tedu.boot22.mapper.HeroMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class HeroController {
    //自动装配注解的作用: Spring框架结合Mybatis框架会自动将HeroMapper生成一个实现类和实现里面的方法
    //而且会自动实例化该对象   required = false告诉idea编译器此对象是非必须的
    @Autowired(required = false)
    HeroMapper mapper;

    @RequestMapping("/add")
    @ResponseBody
    public String add(Hero hero){
        System.out.println("hero = " + hero);
        //此方法相当于之前执行的jdbc代码
        mapper.insert(hero);
        return "添加完成!";
    }

    @RequestMapping("/delete")
    @ResponseBody
    public String delete(String name){
        //掉mapper里面删除的方法
        mapper.deleteByName(name);
        return "删除完成!";
    }

    @RequestMapping("/update")
    @ResponseBody
    public String update(Hero hero){
        System.out.println("hero = " + hero);
        mapper.update(hero);
        return "修改完成!";
    }

    @RequestMapping("/select")
    @ResponseBody
    public String select(){
        List<Hero> list = mapper.select();
        return list.toString();
    }
}
