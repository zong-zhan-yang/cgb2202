package cn.tedu.coolshark.controller;

import cn.tedu.coolshark.entity.Banner;
import cn.tedu.coolshark.mapper.BannerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.List;

@RestController
public class BannerController {

    @Value("${dirPath}")
    private String dirPath;

    @Autowired(required = false)
    BannerMapper mapper;

    @RequestMapping("/banner/select")
    public List<Banner> select(){
        return mapper.select();
    }
    @RequestMapping("/banner/delete")
    public void delete(int id){
        //先查询到轮播图的url
        String url = mapper.selectUrlById(id);
        //  aaa.jpg     F:/files/aaa.jpg
        //得到文件的完整磁盘路径
        String filePath = dirPath+"/"+url;
        //创建文件对象并删除
        new File(filePath).delete();

        mapper.deleteById(id);
    }
    @RequestMapping("/banner/insert")
    public void insert(@RequestBody Banner banner){
        mapper.insert(banner);
    }
}
