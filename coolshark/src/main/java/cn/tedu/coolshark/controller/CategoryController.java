package cn.tedu.coolshark.controller;

import cn.tedu.coolshark.entity.Category;
import cn.tedu.coolshark.mapper.CategoryMapper;
import cn.tedu.coolshark.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CategoryController {
    @Autowired(required = false)
    CategoryMapper mapper;
    @Autowired(required = false)
    ProductMapper productMapper;

    @RequestMapping("/category/select")
    public List<Category> select(){
        return mapper.select();
    }
    @RequestMapping("/category/delete")
    public void delete(int id ){
        System.out.println("删除分类的id = " + id);
        mapper.deleteById(id);
        //删除和该分类相关的商品
        productMapper.deleteByCid(id);

    }

    @RequestMapping("/category/insert")
    public void insert(Category category){
        mapper.insert(category);
    }


}
