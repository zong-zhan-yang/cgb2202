package cn.tedu.coolshark.mapper;

import cn.tedu.coolshark.entity.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductMapper {
    //0是代表浏览量
    @Insert("insert into product values(null,#{title},#{url}," +
            "#{price},#{oldPrice},0,#{saleCount},#{created},#{categoryId})")
    void insert(Product product);

    @Select("select id,title,price,sale_count,url from product")
    @Result(property = "saleCount",column = "sale_count")
    List<Product> select();

    @Select("select url from product where id=#{id}")
    String selectUrlById(int id);
    @Delete("delete from product where id=#{id}")
    void deleteById(int id);

    @Select("select id,title,url,price,old_price,sale_count from product")
    @Result(property = "oldPrice",column = "old_price")
    @Result(property = "saleCount",column = "sale_count")
    List<Product> selectIndex();

    @Select("select id,title,sale_count from " +
            "product order by sale_count desc limit 0,6")
    @Result(property = "saleCount",column = "sale_count")
    List<Product> selectTop();

    @Select("select id,title,url,price,old_price,sale_count from product where category_id=#{cid}")
    @Result(property = "oldPrice",column = "old_price")
    @Result(property = "saleCount",column = "sale_count")
    List<Product> selectByCid(int cid);
//                                                        concat是SQL语句中将多个字符串进行拼接的函数
    @Select("select id,title,url,price,old_price,sale_count from product where title like concat('%',#{wd},'%')")
    @Result(property = "oldPrice",column = "old_price")
    @Result(property = "saleCount",column = "sale_count")
    List<Product> selectByWd(String wd);



    @Select("select id,title,url,price," +
            "old_price,sale_count,created,view_count from product where id=#{id}")
    @Result(property = "oldPrice",column = "old_price")
    @Result(property = "saleCount",column = "sale_count")
    @Result(property = "viewCount",column = "view_count")
    Product selectById(int id);

    @Update("update product set view_count=view_count+1 where id=#{id}")
    void updateViewCount(int id);

    @Delete("delete from product where category_id=#{id}")
    void deleteByCid(int id);
}
