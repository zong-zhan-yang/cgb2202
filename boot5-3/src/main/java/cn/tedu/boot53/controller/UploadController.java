package cn.tedu.boot53.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@RestController
public class UploadController {
    @RequestMapping("/upload")
    public String upload(MultipartFile picFile) throws IOException {
        System.out.println("picFile = " + picFile);
        //得到文件原始文件名  a.jpg
        String fileName = picFile.getOriginalFilename();
        //得到后缀名  从最后一个.出现的位置截取到最后
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        //得到唯一文件名  UUID.randomUUID()得到一个唯一标识符
        fileName = UUID.randomUUID()+suffix;
        System.out.println("文件名:"+fileName);
        //准备保存图片的文件夹路径
        String dirPath = "F:/files";
        File dirFile = new File(dirPath);
        //如果该文件夹不存在 则创建此文件夹
        if (!dirFile.exists()){
            dirFile.mkdirs();//创建文件夹
        }
        //得到文件的完整路径
        String filePath = dirPath+"/"+fileName;
        //把文件保存到此路径   异常抛出
        picFile.transferTo(new File(filePath));
        System.out.println("文件保存完成! 请去此路径检查文件是否存在 "+filePath);

        return fileName;
    }

    @RequestMapping("/remove")
    public void remove(String name){
        String filePath = "F:/files/"+name;
        new File(filePath).delete();//删除文件
    }
}
