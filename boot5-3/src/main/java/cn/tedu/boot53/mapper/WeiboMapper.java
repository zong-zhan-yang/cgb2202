package cn.tedu.boot53.mapper;

import cn.tedu.boot53.entity.Weibo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface WeiboMapper {
    @Insert("insert into weibo values" +
            "(null,#{content},#{url},#{nick},#{created},#{userId})")
    void insert(Weibo weibo);
    //查询所有微博数据 并且降序排序
    @Select("select id,content,url,nick from weibo order by created desc")
    List<Weibo> select();
}
